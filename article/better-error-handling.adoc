= Better Error Handling & Domain Modeling using Kotlin and Functional Programming.
Andries Spies <andries.spies@gmail.com>
v1.0 2019-01-08

Runtime error processing on the JVM based platforms has always been less than ideal. Especially with the Java language in mind. To illustrate this point, consider the following typical java snippet:

.Illustrative Java Sample
[source,java]
----
class Invoice {

    void export(OutputStream dest,
                ExportFormat format, 
                Signature signature) 
                throws 
                    IOException, // <1>
                    FormatException {
                    
        ExportFormat.Writer writer = format.writer(dest); 
        
        try {
            writer.write(new Invoice.Attribute("export_date", LocalDateTime.now());
            writeDetails(writer);
            writeLineItems(writer); 
            writeTotals(writer);
            writeAgeAnalysis(writer);
            if (signature != null) {
                writer.signContent(signature); // <2>
            }
        } catch (IOException) {
            log.warn("Failed to export invoice {}",id);  // <3> 
            throw e;
        } finally {
            writer.close();
        }
    }

}

// Using it: 
try(OutputStream out = new FileOutputStream("invoice.json.gz") {
    invoice.export(
        out, 
        ExportFormat.Json().withGzip(), 
        new FileSignature("sign.dat"))
}
----
<1> Any of the underlying libraries could expose an IOException.
<2> Case in point: Signature may fail to read, maybe this is not important, but we fail here.
<3> At this point we do not know which part of the code has failed because the flow has been broken - have to wait now for a stacktrace at runtime to make a decision.

To be fair, this is not as much a Java problem, but also a _C#_, _Python_, _C++_, or any language which employs the `try-catch { ... }` construct as a language feature. 

The problem an be summed up one one simple statement of fact:

[IMPORTANT]
====
The moment we end up the the _catch_ part of the statement, we have just lost some, (if not most) of the control over the flow in our application.
====

There are a few solutions to regain control here. 

Solution 1:: We could just wrap each all in into a `try-catch` call. But that would be also less ideal on at least 3 fronts:
    1. We can forget to throw it, thus hiding a legitimate error.
    2. If would obscure the intention of of the code.
    3. Come 6 months down the line, everyone, including the original developer would have forgotten why the code morphed into this
Solution 2:: Use codes, but this presents us with:
    1. We can still eat up exceptions.
    1. Codes tend to be global, and thus have to maintained outside of the scope where they are used.
    1. There is a definite benefit to have global exception handler, which we should not just thrown out of the door.
    1. Poor interoperability with a modern language runtime such as Java.
    1. Massive redesign of APIs -- not always feasible, nor desired.
Solution 3:: Add some manual intervention, and pray that we have enough unit tests exercising our edge cases:
    1. Not ideal as now we have to wait for some runtime condition. 
    1. We still cannot reason about the flow really, as the flow will be obscured once we encounter edge cases, and sprinkle our code with more `try-catch` statements.
    

== Having our Cake and eating it.

There is a better way. We can take some lessons from functional programming. Specially, we have a look at construct called an `Attempt<A,B>`, (which is a specialised adopted `Either` monad). Basically we have either an value of type `<A>`, or value of type `<B>`. By convention `A` represents the a failure (in our case an exception), and `B` an expected value/result.

Implementing the basic requirement is easy using a sealed class hierarchy:

.Attempt.kt
[source,kotlin]
----
sealed class Attempt<out E,out T> {

    data class Expected<T>(val value: T) : Attempt<Nothing, T>()

    data class Failure<E>(val value: E) : Attempt<E, Nothing>()

    override fun toString(): String {
        return when (this) {
            is Expected -> "expected: $value"
            is Failure -> "failed-with: $value"
        }
    }
}
----

Next we need a convenient way to access this class hierarchy.

[source,kotlin]
----
val <E : Throwable, T> Attempt<E, T>.expected: T
    get() = when (this) {
        is Attempt.Expected -> value
        is Attempt.Failure -> throw value // <1>
    }

val <E, T> Attempt<E, T>.error: E? get() = (this as? Attempt.Failure)?.value <2>

inline val <reified E, T> Attempt<E, T>.value: T <3>
    get() = when (this) {
        is Attempt.Expected -> value
        is Attempt.Failure ->
            when (value) {
                is Throwable -> throw value
                else -> throw NoSuchElementException(
                    "Missing value: $value")
            }
    }

----
<1> Throw exception instead.
<2> Return optional error value.
<3> Access the expected value, but will always raise an exception if there nothing (even of the error is not an `Throwable`).

I choose to use extension properties for the following reasons:

Consideration related to expected values: :: Access the underling expected value in the case of exception should raise the exception instead, because:
    - This is the expected behaviour most developers would expect.
    - It still forces us to deal with the exception.
    - There is no need to check for a special `null`
    - It is possible to return the underlying exception, or a `null` if desired.
    - There is less change of our code eating up the exception.
Considerations related to errors/exceptions: :: An failure, although catering for it, is not expected, so we just return an null in the absence of it, and as a plus:
    - Kotlin has excellent null support, which makes it a more ideal choice for the language.
    - Allows to cater for ad-hock exception handling.

Of cause, we still have to wrap calls, for this we using the following extension functions:

[source,kotlin]
----
fun <T> T.asExpected(): Attempt.Expected<T> = Attempt.Expected(this) 
fun <E> E.asFailure(): Attempt.Failure<E> = Attempt.Failure(this) 
----

Using these functions, we can easily use what either return an `Attempt.Expected`, or wrap an existing call into an `Attempt.Failure`.

This brings us:

=== Improved Exception Handling

A typical use would be something such a:

[source,kotlin]
----
val inputAttempt = try {                          // <1>
            FileInputStream(file).asExpected()
        } catch(e: IOException) {
            e.asFailure()
        }
        

// Using it:
val data = inputAttempt.expected.use {             // <2>
    // ... process input here             
}
----
<1> Wrap it up in either an expected value, or error.
<2> Use it, but application will raise an exception if the open attempt failed.

This boiler plate the `try-catch` statement can reduced by capturing the logic into top a level functions, which may look like this:

[source,kotlin]
----
val inputAttempt = attempt<IOException,InputStream> { FileInputStream(file) }
val data = inputAttempt.expected.use {
    // process input
}
----

These are simple examples. But the real gain is when we have to handle exceptions across different domains, of which some reside in 3^rd^ party libraries. 

Here is such complex example which employs mapping of exceptions from one domain into another:

[source,kotlin]
----

// 3rd Party API:
interface NamedSocketOpener {
    fun open(socketName: String): Socket
}

// Some production code:
val openedSocketAttempt = attemptAny({ namedSocketOpener.open("!") }) { // <1>
    when (it) {
        is IllegalStateException -> EOFException("socket has been closed").causeBy(it) <2> 
        else -> IOException("Error while opening socket.").causeBy(it)
    }
}

----
<1> We found some oddities in using the librar, so we handle exceptions our way.
<2> For some reason they rather throw an `IllegalStateException` when socket closed, wierd, but we can handle it.

=== Improved API 

It is common for any extensive set of domain components to have model hierarchy of domain specific exceptions.
A good example of such would be the data access exception hierarchy found in the Spring Framework.

Consider a domain modelling of typical a key-value store:

[source,kotlin]
----

interface KeyValueStore {

    abstract class StoreException(message: String) : Exception(message)

    fun contains(key: String): Attempt<StoreException, Boolean> // <1>
    fun <T> get(key: String, type: Class<T>): Attempt<StoreException, T> // <1>
    fun <T> put(key: String, type: Class<T>, value: T): Any? // <2>
    fun remove(key: String): Attempt<StoreException, Any?>
}

inline fun <reified T> KeyValueStore.put(key: String, value: T): Any? 
        = put(key, T::class.java, value) // <3>
----
<1> Retrieving a value, we can delay errors until the caller retrieves it. 
<2> A modification should fail as soon as possible, as we modify state. 
<3> Convenience extension function which passes in the actual type.

Now consider accessing this API, and notice:

[source,kotlin]
----
stores.open("my.ks1").use  { store -> <2>
    
    if (!store.contains("name").value) {  // <1>
        store.put("name",name)
    }

    val dateOfBirth : LocalDateTime = store.get("dob")
}
----
<1> No need to check since we are in the safe area 
<2> Access the underlying value directly

== Summary

What we gained: ::
    In terms of error handling: :::
        1. We can delay error handling to a more convenient point in our application flow.
        1. Accessing the value at that point will only still, as expected, throw an exception if the call has actually failed.
        1. We can still check errors at any time.
        1. We do not have to work inside a `try-catch` block footnote:[This is handled by the top level function which wraps our call result into an attempt.].
    In terms API of modelling: :::
        1. We can model domain errors on API level.
        1. We still, do not force our users of the API to use `try-catch`, exception if they do choose to not do any error checking. 
        1. We can easy remap one type of domain error into another across different domains, or while accessing 3^rd^ party library code.



