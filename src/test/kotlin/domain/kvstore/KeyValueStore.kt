package domain.kvstore

import attempt.Attempt

interface KeyValueStore {

    abstract class StoreException(message: String) : Exception(message)

    fun contains(key: String): Attempt<StoreException, Boolean>
    fun <T> get(key: String, type: Class<T>): Attempt<StoreException, T>
    fun <T> put(key: String, type: Class<T>, value: T): Any?
    fun remove(key: String): Attempt<StoreException, Any?>
}

inline fun <reified T> KeyValueStore.put(key: String, value: T): Any?
        = put(key, T::class.java, value)