package domain.survey

import java.util.*

data class SurveyRecord(
    override val surveyId: String = UUID.randomUUID().toString(),
    override val name: String,
    override val purpose: String
) : SurveyManager.Survey