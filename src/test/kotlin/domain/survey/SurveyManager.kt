package domain.survey

import attempt.Attempt

interface SurveyManager {

    interface Survey {
        val surveyId: String
        val name: String
        val purpose: String
    }

    sealed class SurveyException(message: String) : Exception(message) {
        class DuplicateSurvey(message: String) : SurveyException(message)
        class CreationFailed(message: String) : SurveyException(message)
    }

    fun createSurvey(ownerId: String, name: String, purpose: String): Attempt<SurveyException, Survey>

}