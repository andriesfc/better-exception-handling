package infrastructure

import java.net.Socket

interface NamedSocketOpener {
    fun open(socketName: String): Socket
}