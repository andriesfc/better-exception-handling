package attempt

import domain.survey.SurveyManager
import domain.survey.SurveyManager.Survey
import domain.survey.SurveyManager.SurveyException
import domain.survey.SurveyManager.SurveyException.CreationFailed
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.fail
import java.io.FileNotFoundException
import java.io.IOException


@DisplayName("Complex Failure Handling")
@ExtendWith(MockKExtension::class)
class ComplexFailureHandling {

    @MockK
    private lateinit var surveyManager: SurveyManager

    @Test
    @DisplayName("Handing leaked cross domain failures (using dsl function).")
    fun handlingLeakedInfrastructureFailures() {

        surveyManager.createSurveyShouldFailBecauseOfLeakedIOException()

        val surveyOwnerId = "owner"
        val surveyName = "name"
        val surveyDescription = "purpose"

        val attemptedSurvey: Attempt<SurveyException, Survey> = surveyManager.run {
            attempt(
                attempt = {
                    createSurvey(
                        ownerId = surveyOwnerId,
                        purpose = surveyDescription,
                        name = surveyName
                    ).expected
                },
                mapFailure = {
                    CreationFailed(
                        "Failure while creating survey named `$surveyName` (owned by `$surveyOwnerId`)"
                    ).causeBy(it)
                }
            )
        }

        val failure = attemptedSurvey.error ?: fail("Expected failure.")

        assertTrue(failure.cause is IOException)
    }


    companion object {
        private fun SurveyManager.createSurveyShouldFailBecauseOfLeakedIOException() {
            every {
                createSurvey(
                    ownerId = any(),
                    name = any(),
                    purpose = any()
                )
            } throws FileNotFoundException("Unable to template file.")
        }
    }

}