package attempt

import domain.survey.SurveyManager
import domain.survey.SurveyManager.SurveyException.CreationFailed
import domain.survey.SurveyManager.SurveyException.DuplicateSurvey
import domain.survey.SurveyRecord
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.math.BigInteger
import kotlin.random.Random

@DisplayName("Use to provided cleaner and useful domain specific API.")
@ExtendWith(MockKExtension::class)
class CleanerDomainApi {

    @MockK
    lateinit var surveyManager: SurveyManager

    @Test
    @DisplayName("Result which clearly indicate that result may be an domain specific failure.")
    fun adiIndicatingDomainSpecificFailure() {

        surveyManager.createSurveyShouldReport(DuplicateSurvey("Survey with name already exists."))

        val createSurveyAttempt = surveyManager.createSurvey("owner", "test", "purpose")

        assertTrue(createSurveyAttempt.error is DuplicateSurvey)
    }

    @Test
    @DisplayName("Caller have choice but does not mean domain errors are thrown away.")
    fun callerCanStillChooseToHandleFailureButNotEatingIfNotHandled() {

        // Business rule: Create mangled unique name of survey exists already.
        val ownerId = "me"
        val surveyName = "test"
        val surveyDescription = "purpose"
        val mangledName = "description671cd1"

        surveyManager.createSurveyShouldReport(CreationFailed("Entity $ownerId is not allowed to create surveys."))

        assertThrows(CreationFailed::class.java) {
            with(surveyManager) {
                createSurvey(ownerId, surveyName, surveyDescription).run {
                    when (error) {
                        is DuplicateSurvey -> createSurvey(ownerId, mangledName, surveyDescription).expected
                        else -> expected
                    }
                }
            }
        }
    }

    @Test
    @DisplayName("Complex caller logic based on possible domain failures.")
    fun complexCallerLogicBasedOnDomainFailures() {

        // Business rule: Create mangled unique name of survey exists already.
        val ownerId = "me"
        val surveyName = "test"
        val surveyDescription = "purpose"
        val mangledName = "description671cd1"

        surveyManager.createSurveyShouldReportDuplicate(surveyName)

        val survey = with(surveyManager) {
            createSurvey(ownerId, surveyName, surveyDescription).run {
                when (error) {
                    is DuplicateSurvey -> createSurvey(ownerId, mangledName, surveyDescription).expected
                    else -> expected
                }
            }
        }

        verify { surveyManager.createSurvey(ownerId = ownerId, name = surveyName, purpose = surveyDescription) }
        verify { surveyManager.createSurvey(ownerId = ownerId, name = mangledName, purpose = surveyDescription) }

        println(survey)
    }

    companion object {

        private fun SurveyManager.createSurveyShouldReport(exception: SurveyManager.SurveyException) {
            every { createSurvey(any(), any(), any()) } returns exception.asFailure()
        }


        private fun SurveyManager.createSurveyShouldReportDuplicate(existingSurveyName: String) {

            every { createSurvey(any(), any(), any()) } answers {

                val name = arg<String>(1)
                val description = arg<String>(2)
                val nextId = { BigInteger(Random.nextBytes(10)).toString(32) }

                attempt {

                    if (name == existingSurveyName) {
                        throw DuplicateSurvey("Duplicate survey found: $existingSurveyName")
                    }

                    SurveyRecord(
                        surveyId = nextId(),
                        name = name,
                        purpose = description
                    )
                }
            }
        }
    }
}