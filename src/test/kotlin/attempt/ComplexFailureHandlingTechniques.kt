package attempt

import infrastructure.NamedSocketOpener
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockkClass
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.io.EOFException
import java.io.IOException
import java.io.InputStream
import java.lang.IllegalStateException
import java.lang.RuntimeException
import java.net.Socket
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.random.Random


@DisplayName("Demonstrate more complex error handling techniques")
@ExtendWith(MockKExtension::class)
class ComplexFailureHandlingTechniques {

    @MockK
    private lateinit var namedSocketOpener: NamedSocketOpener

    @Test
    @DisplayName("Wrap all possible exception at invocation.")
    fun wrappingExceptionAtInvocation() {

        setupMockSocketOpener()

        val openedSocketAttempt = attemptAny({ namedSocketOpener.open("!") }) {
            when (it) {
                is IllegalStateException -> EOFException("socket has been closed").causeBy(it)
                else -> IOException("Error while opening socket.").causeBy(it)
            }
        }

        assertNotNull(openedSocketAttempt.error, "Expected an IOException")
        assertTrue(openedSocketAttempt.error?.cause is RuntimeException)

    }


    private fun setupMockSocketOpener() {

        every { namedSocketOpener.open(any()) } answers {

            val validSocketNameRegex = Regex("[a-zA-Z]+\\d*[-_:/\\\\@.]*")
            val socketName: String = arg(0)

            when {
                socketName.isEmpty() -> throw IllegalArgumentException("You have to supply a socket name.")
                socketName.isBlank() -> throw IllegalArgumentException("Blank socket names are not allowed.")
                !socketName.matches(validSocketNameRegex) -> throw IllegalArgumentException("A socket name may only consist of, upper and lower case letters a to z. Only special characters which are allowed are: `-`, `_`, `:`, `/`, `@`, and  `\\`.")
                else -> mockkClass(Socket::class).apply {

                    class InfiniteGarbageInputStream : InputStream() {
                        private val closed = AtomicBoolean(false)
                        override fun read(): Int = when (val available = !closed.get()) {
                            available -> Random.nextInt(0)
                            else -> throw EOFException()
                        }
                    }

                    var closed = false
                    val input by lazy { InfiniteGarbageInputStream() }
                    fun <T> checkedIO(io: () -> T): T =
                        if (closed) throw IllegalStateException("Socket \"$socketName\" has been closed.") else io()
                    every { close() } answers {
                        if (!closed) {
                            closed = true
                            input.close()
                        }
                    }
                    every { getInputStream() } answers { checkedIO { input } }
                    every { getOutputStream() } answers { checkedIO { System.out } }
                }
            }
        }
    }
}