package attempt

import infrastructure.NamedSocketOpener
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.io.IOException
import java.net.Socket


@DisplayName("Examples of how to produce instances of the `Attempt`")
class ProducingUseCases {


    private val namedSocketOpener = mockk<NamedSocketOpener>(relaxed = true)

    @Test
    @DisplayName("Using try-catch construct to report an attempt.")
    fun produceWithTryCatch() {

        namedSocketOpener.mockOpenShouldThrowIOException()

        val openSocketAttempt = try {
            namedSocketOpener.open("socket1").asExpected()
        } catch (e: Exception) {
            e.asFailure()
        }

        println(openSocketAttempt)

        verify { namedSocketOpener.open(any()) }
        assertNotNull(openSocketAttempt.error, "Expected an error to be captured.")
        assertTrue(openSocketAttempt.error is IOException, "Expected an IO exception has an error but found ${openSocketAttempt.error?.javaClass?.name} instead.")
    }

    @Test
    @DisplayName("Using the attempt DSL provided.")
    fun produceWithAttemptDSL() {

        namedSocketOpener.mockOpenShouldThrowIOException()

        val openSocketAttempt = attempt<IOException,Socket> { namedSocketOpener.open("socket1") }
        println(openSocketAttempt)

        verify(atMost = 1) { namedSocketOpener.open(any()) }
        assertNotNull(openSocketAttempt.error, "Expected an error to be captured.")
        assertTrue(openSocketAttempt.error is IOException, "Expected an IO exception has an error but found ${openSocketAttempt.error?.javaClass?.name} instead.")
    }

    companion object {

        private fun NamedSocketOpener.mockOpenShouldThrowIOException() {
            every { open(any()) } answers { throw IOException("Unable to open named socket: ${arg<String>(0)}") }
        }

    }
}