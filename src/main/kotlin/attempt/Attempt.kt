package attempt

/**
 * An specialised functional pattern (based the Either monad), to handle an exception where a result is a function
 * can either be [expected] value, or an optional [error].
 *
 * @param E Placeholder for failure type.
 * @param T Placeholder for an expected type.
 *
 * @see asExpected
 * @see asFailure
 * @see expected
 * @see error
 */
sealed class Attempt<out E, out T> {

    /**
     * Value holder for an expected value.
     */
    data class Expected<T>(val value: T) : Attempt<Nothing, T>()

    /**
     * Value holder for an failure.
     */
    data class Failure<E>(val value: E) : Attempt<E, Nothing>()

    override fun toString(): String {
        return when (this) {
            is Expected -> "expected: $value"
            is Failure -> "failed-with: $value"
        }
    }
}

/**
 * Converts a receiver into an instance of an [Attempt.Expected].
 */
fun <T> T.asExpected(): Attempt.Expected<T> = Attempt.Expected(this)

/**
 * Converts a receiver into an instance of an [Attempt.Failure].
 */
fun <E> E.asFailure(): Attempt.Failure<E> = Attempt.Failure(this)

/**
 * Gets the expected value, or throws the [Attempt.Failure.value] if an exception has been thrown.
 */
val <E : Throwable, T> Attempt<E, T>.expected: T
    get() = when (this) {
        is Attempt.Expected -> value
        is Attempt.Failure -> throw value
    }

inline val <reified E, T> Attempt<E, T>.value: T
    get() = when (this) {
        is Attempt.Expected -> value
        is Attempt.Failure ->
            when (value) {
                is Throwable -> throw value
                else -> throw NoSuchElementException("Value not available, instead found error=$value")
            }
    }


/**
 * Gets the [Attempt.Failure.value] the receiver is not of [Attempt.Expected] type, or a `null` value.
 */
val <E, T> Attempt<E, T>.error: E? get() = (this as? Attempt.Failure)?.value

/**
 * Attempts to attempt an expected value of [Attempt.Expected], or in the case of an exception being
 * raised, captures any exception of type [E] and returns a [Attempt.Failure]
 *
 * @param attempt A function to attempt a value of type [T].
 * @param T The expected value type.
 * @param E An allowed `Throwable` type.
 */
inline fun <reified E : Throwable, T> attempt(attempt: () -> T): Attempt<E, T> {
    return try {
        attempt().asExpected()
    } catch (e: Throwable) {
        (e as? E)?.asFailure() ?: throw e
    }
}

inline fun <reified E : Throwable, T> attempt(attempt: () -> T, mapFailure: (Throwable) -> E?): Attempt<E, T> {
    return try {
        attempt().asExpected()
    } catch (raised: Throwable) {
        val translated = mapFailure(raised) ?: throw raised
        translated.asFailure()
    }
}


inline fun <reified E : Throwable, T> attemptAny(attempt: () -> T, mapFailure: (Throwable) -> E): Attempt<E, T> {
    return try {
        attempt().asExpected()
    } catch (e: Throwable) {
        when (e) {
            is E -> e.asFailure()
            else -> mapFailure(e).asFailure()
        }
    }
}


/**
 * Folds an [Attempt] into single type of [R]
 *
 * @param foldError A function which takes an error instance of [E] and returns an instance of [R].
 * @param foldExpected A function which takes an instance of [T] and maps it to an instance of [R]
 * @param E Type placeholder for error value
 * @param T Type placeholder for an expected value
 * @param R Type placeholder for return type.
 *
 * @receiver A previous result of an attempted operation.
 * @return A instance of [R]
 */
fun <E, T, R> Attempt<E, T>.fold(foldError: (E) -> R, foldExpected: (T) -> R): R {
    return when (this) {
        is Attempt.Expected -> foldExpected(value)
        is Attempt.Failure -> foldError(value)
    }
}

fun <E, E1, T> Attempt<E, T>.mapFailure(transformFailure: (E) -> E1): Attempt<E1, T> {
    return fold({ transformFailure(it).asFailure() }, { it.asExpected() })
}

fun <E, T, T1> Attempt<E, T>.map(transform: (T) -> T1): Attempt<E, T1> {
    return fold({ it.asFailure() }, { transform(it).asExpected() })
}

fun <E : Throwable> E.causeBy(another: Throwable): E = apply { initCause(another) }
